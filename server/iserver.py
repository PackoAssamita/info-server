#!/usr/bin/env python
# -*- coding: UTF8 -*-
# Copyright (C) 2005-2014 by
#   Francisco José Moreno Llorca <packo@assamita.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

from SimpleXMLRPCServer import *
#import thread
from time import sleep
from thread import *
from os import *
import sys


hd = '/dev/hda'

def cpu_funcion(cpu_datos, cpu_da):
    """
        threaded function
    """
    old_use = 0
    old_sys = 0
    use = 0
    system = 0
    while 1:
        linea = file('/proc/stat').read().split('\n')[0]
        lista = linea.split()
        use = int(lista[1])
        system = int(lista[3])
        cpu_datos.acquire()
        cpu_da[0] = use - old_use
        cpu_da[1] = system - old_sys
        cpu_datos.release()
        old_use = use
        old_sys = system
        sleep(3)        
    
    
class sesiones:
    def __init__(self,cps,cpd):
        print "Opening sessions"
        self.cpu_datos = cps
        self.cpu_da = cpd
        
    def salir(self):
        print "Exit"
        sys.exit(sys.EX_OK)
        
    def info_sistema(self):
        """
            @return a list of elements : nombre_host, nombre dominio,kernel.
        """
        hostname = file('/proc/sys/kernel/hostname').read()
        domainname = file('/proc/sys/kernel/domainname').read()
        if domainname == '(none)\n':
            domainname = 'No tiene'
        kernel = file('/proc/sys/kernel/osrelease').read()
        return [hostname.split('\n')[0],domainname,kernel.split('\n')[0]]
    
    def info_cpu(self):
        """
            @return a list of elements: vendor_id, nombre_modelo, mhz, cache
        """
        archivo = file('/proc/cpuinfo').read()
        vendor_id = archivo.split('\n')[1].split(': ')[-1]
        modelo = archivo.split('\n')[4].split(': ')[-1]
        mhz = archivo.split('\n')[6].split(': ')[-1].split('.')[0]
        cache = archivo.split('\n')[7].split(': ')[-1]
        return [vendor_id,modelo, mhz,cache]
        
    def info_syslog(self,linea='syslogd'):
        """
            @return syslog output
        """ 
        try:
            archivo = file('/var/log/messages').read()
        except IOError:
            archivo = file('/var/log/syslog').read()
        
        lineas = archivo.splitlines()
        salida=[]
        
        contador = -1
        while (1):
            
            if lineas[contador].find(linea) != -1:
                break
            contador = contador -1
        for a in range(contador+1, -1):
            salida.append(lineas[a].split(' ',5))
        return salida
            
    def info_modules(self):
        """ @return lsmod info
        """
        b = popen('lsmod','r')
        sal = b.read()
        b.close()
        lineas = sal.splitlines()
        salida=[]
        line = []
        for a in lineas[1:]:
            line = []
            line.append( a.split()[0])
            aux = a.split(' ',1)
            aux = aux[1].strip()
            lista = aux.split()
            line.append(lista[0])
            if len(lista) == 3:
                line.append(lista[-1])
            else :
                line.append(" ")
            salida.append(line)
        return salida
        
    def info_devices(self):
        pass

    def datos_iniciales(self):
        """ 
            @return a list of elements: espacio a, espacio b,mem_total, swap_total
        """
        lista = file('/proc/meminfo').read().split()
        b = popen('df / -h','r')
        sal = b.read()
        b.close()
        lista1 = sal.split()
        b = popen('df /home/ -h','r')
        sal = b.read()
        b.close()
        lista2 = sal.split()
        #start smart:
        #system('smartctl -s on '+hd)
        return [lista1[8][0:-1],lista2[8][0:-1],lista[1],lista[34]] #TODO: check

    def cpu(self):
        """
            @return the % of use of cpu1 and the system load in a two elements list
        """
        self.cpu_datos.acquire()
        uso = self.cpu_da[0]
        sys = self.cpu_da[1]
        self.cpu_datos.release()
        return [uso,sys]
    
    def espacio_2(self):
        """
            @return a list of elements: a:ocupado,b:ocupado in % format
        """
        
        b = popen('df / -h','r')
        sal = b.read()
        b.close()
        lista1 = sal.split()
        b = popen('df /home/ -h','r')
        sal = b.read()
        b.close()
        lista2 = sal.split()
        return [lista1[11][0:-1],lista2[11][0:-1]]

    def memoria(self):
        """
            @return a list of elements: libre, cached, buffered,swap_cached in kb
        """
        lista = file('/proc/meminfo').read().split()

        return [lista[4],lista[7],lista[10],lista[13]]
    
    def disco_temp(self):
        """
            @return the hdd temp
        """
        tempe = popen( 'smartctl --attributes '+hd+' |grep Temperature_Celsius','r')
        temp_hd = tempe.read().split()[9]
        tempe.close()
        return temp_hd
        
class servidor:
    """main class"""
    configuraciones = {'puerto':6051, 'version_cliente':0.1,'version_server':0.1}
    
    
    def __init__(self):
        #self.leer_configuraciones()
        #oldstderr=sys.stderr

        sys.stderr=file('infoserver.log', 'w')
        self.server = SimpleXMLRPCServer(("", self.configuraciones['puerto']))
        cpu_datos = allocate_lock()
        cpu_datos.acquire()
        cpu_da = [0,0]
        cpu_datos.release()

        self.server.register_instance(sesiones(cpu_datos,cpu_da))

        a = cpu_datos,cpu_da
        start_new_thread(cpu_funcion, a)
        
    def run(self):
        self.server.serve_forever()

if __name__ == "__main__":
    a = servidor()
    a.run()
    
