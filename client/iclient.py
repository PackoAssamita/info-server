#!/usr/bin/env python
# -*- coding: UTF8 -*-
# Copyright (C) 2005-2014 by
#   Francisco José Moreno Llorca <packo@assamita.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#to 0.2 los controles que no se expanden no funcionan ni hacen las peticiones

import pygtk
pygtk.require('2.0')
import gtk
import gtk.glade
import gtk.gdk
import thread
from xmlrpclib import *
import time

gtk.gdk.threads_init()
gtk.gdk.threads_enter()


class iclient:
    TIME_UPDATE = 1
    TIME_UPDATE_LENTO = 10
    ult_syslog = ""
    url_server = "http://localhost"
    port_server= "6051"
    salir = False
    def __init__(self):
        self.x = gtk.glade.XML('gui.glade')
        self.x.signal_autoconnect(self)
        self.server = ServerProxy(self.url_server+":"+self.port_server)
        self.b_hda = self.x.get_widget('progressbar11')
        self.b_home = self.x.get_widget('progressbar12')
        self.b_use = self.x.get_widget('progressbar13')
        self.b_sys = self.x.get_widget('progressbar14')
        self.b_mem = self.x.get_widget('progressbar10')
        self.b_cached = self.x.get_widget('progressbar7')
        self.b_buffer = self.x.get_widget('progressbar8')
        self.b_swap = self.x.get_widget('progressbar9')
        self.l_use = self.x.get_widget('l_uti')
        self.l_cached = self.x.get_widget('l_cach')
        self.l_buffer = self.x.get_widget('l_buff')
        self.l_swap = self.x.get_widget('l_swap')
        self.l_raiz_usado = self.x.get_widget('l_raiz_usado')
        self.l_home_usado = self.x.get_widget('l_home_usado')
        self.l_host = self.x.get_widget('l_host')
        self.l_dominio = self.x.get_widget('l_dominio')
        self.l_kernel = self.x.get_widget('l_kernel')
        self.l_fabri = self.x.get_widget('l_fabri')
        self.l_modelo = self.x.get_widget('l_modelo')
        self.l_cache = self.x.get_widget('l_cache')
        self.l_velo = self.x.get_widget('l_velo')
        self.a_syslog = self.x.get_widget('a_syslog')
        self.a_modulos = self.x.get_widget('a_modulos')
        
        # define syslog columns
        self.model_syslog = gtk.ListStore(str,str,str,str)
        self.a_syslog.set_model(self.model_syslog)
        
        self.csys_hora = gtk.TreeViewColumn("Time")
        self.csys_objeto = gtk.TreeViewColumn("Object")
        self.csys_mensaje = gtk.TreeViewColumn("Message")
        self.csys_fecha = gtk.TreeViewColumn("Date")
        
        
        self.a_syslog.append_column(self.csys_hora)
        self.a_syslog.append_column(self.csys_objeto)
        self.a_syslog.append_column(self.csys_mensaje)
        self.a_syslog.append_column(self.csys_fecha)
        self.cella = gtk.CellRendererText()
        self.csys_hora.pack_start(self.cella, True)
        self.csys_objeto.pack_start(self.cella, True)
        self.csys_mensaje.pack_start(self.cella, True)
        self.csys_fecha.pack_start(self.cella, True)
        self.csys_hora.add_attribute(self.cella, 'text', 0)
        self.csys_objeto.add_attribute(self.cella, 'text', 1)
        self.csys_mensaje.add_attribute(self.cella, 'text', 2)
        self.csys_fecha.add_attribute(self.cella, 'text', 3)
        # start syslog control
        try:
            a = self.server.info_syslog()
            self.ult_syslog = a[-1][-1]
            for lin in a:
                self.model_syslog.append([lin[2],lin[4],unicode(lin[5]),lin[0]+' '+lin[1]])
        except:
            a = []

        # modules column definition
        self.model_modules = gtk.ListStore(str,str,str)
        self.a_modulos.set_model(self.model_modules)
        
        self.cmod_nombre = gtk.TreeViewColumn("Module")
        self.cmod_tamano = gtk.TreeViewColumn("Size")
        self.cmod_usado= gtk.TreeViewColumn("Used by")
        #self.cmod_numero = gtk.TreeViewColumn("Numero")
        
        
        self.a_modulos.append_column(self.cmod_nombre)
        self.a_modulos.append_column(self.cmod_tamano)
        self.a_modulos.append_column(self.cmod_usado)
        #self.a_syslog.append_column(self.csys_fecha)
        self.cellb = gtk.CellRendererText()
        self.cmod_nombre.pack_start(self.cellb, True)
        self.cmod_tamano.pack_start(self.cellb, True)
        self.cmod_usado.pack_start(self.cellb, True)
        #self.csys_fecha.pack_start(self.cellb, True)
        self.cmod_nombre.add_attribute(self.cellb, 'text', 0)
        self.cmod_tamano.add_attribute(self.cellb, 'text', 1)
        self.cmod_usado.add_attribute(self.cellb, 'text', 2)
        #self.csys_fecha.add_attribute(self.cellb, 'text', 3)
        # start modules control
        a = self.server.info_modules()
        for lin in a:
            self.model_modules.append([lin[0],lin[1],lin[2]])
        
        d = self.server.datos_iniciales()
        self.hda_t = d[0]
        self.home_t = d[1]
        self.mem_t = float(d[2])
        self.swap_t = float(d[3])
        sistema = self.server.info_sistema()
        self.l_host.set_text(sistema[0])
        self.l_dominio.set_text(sistema[1])
        self.l_kernel.set_text(sistema[2])
        sistema = self.server.info_cpu()
        self.l_fabri.set_text(sistema[0])
        self.l_modelo.set_text(sistema[1])
        self.l_velo.set_text(sistema[2]+" Mhz")
        self.l_cache.set_text(sistema[3])
    
        # initial setup
        self.cpu = True
        self.mem = True
        self.hd = True
        self.info = False

        self.x.get_widget('l_memoria').set_text('Memory: '+str(int(self.mem_t)/1024)+'MB  &  Swap: '+str(int(self.swap_t)/1024)+'MB')
        
        # launch the threads
        self.hebra = thread.start_new_thread(self.refresca_hebra,(self.TIME_UPDATE,))
        self.hebra2 = thread.start_new_thread(self.refresca_hebra_lenta,(self.TIME_UPDATE_LENTO,))

    def refresca_hebra(self,timewait):
        gtk.gdk.threads_leave()
        while(self.salir == False):
            # update hdd
            if self.hd ==True:
                a = self.server.espacio_2()
                gtk.gdk.threads_enter()
                self.b_hda.set_fraction(float(a[0])/100)
                self.b_home.set_fraction(float(a[1])/100)
                self.l_raiz_usado.set_text(str(float(self.hda_t)*1024)+'MB')
                self.l_home_usado.set_text(self.home_t+'GB')
                gtk.gdk.threads_leave()
            
            # update cpu
            if self.cpu == True:
                c = self.server.cpu()
                gtk.gdk.threads_enter()
                self.b_use.set_fraction(float(c[0])/100)
                self.b_sys.set_fraction(float(c[1])/100)
                gtk.gdk.threads_leave()
            # update memory
            if self.mem == True:
                b = self.server.memoria()
                gtk.gdk.threads_enter()
                self.b_mem.set_fraction((self.mem_t - float(b[0]))/self.mem_t)
                self.b_buffer.set_fraction(float(b[1])/self.mem_t)
                self.b_cached.set_fraction(float(b[2])/self.mem_t)
                try:
                    self.b_swap.set_fraction(float(b[3])/self.swap_t)
                except:
                    self.b_swap.set_fraction(0)

                self.l_use.set_text(str(int(self.mem_t - float(b[0]))/1024)+'MB')
                self.l_buffer.set_text(str(int(b[1])/1024)+'MB')
                self.l_cached.set_text(str(int(b[2])/1024)+'MB')
                self.l_swap.set_text(str(int(b[3])/1024)+'MB')
                gtk.gdk.threads_leave()
            
            time.sleep(timewait)
        return 1
    
    def refresca_hebra_lenta(self,timewait):
        gtk.gdk.threads_leave()
        while(self.salir == False):
            if self.info == True:
            # fix syslog expand
                a = self.server.info_syslog(self.ult_syslog)
                try:
                    self.ult_syslog = a[-1][-1]
                    for lin in a:
                        gtk.gdk.threads_enter()
                        self.model_syslog.append([lin[2],lin[4],lin[5],lin[0]+' '+lin[1]])
                        gtk.gdk.threads_leave()
                except Exception,e:
                    print e
    
                # modules
                a = self.server.info_modules()
                gtk.gdk.threads_enter()
                self.model_modules.clear()
                for lin in a:
                    self.model_modules.append([lin[0],lin[1],lin[2]])
                gtk.gdk.threads_leave()
            time.sleep(timewait)
        return 1
            
    def run(self):
        gtk.main()
    def on_window1_delete_event(self, *args):
        self.salir = True
        try:
            self.server.salir()
        except:
            pass
        gtk.main_quit()
        
    def on_expandercpu_activate(self, *args):
        if self.cpu == True:
            self.cpu =False
        else:
            self.cpu =True
    def on_expandermem_activate(self, *args):
        if self.mem == True:
            self.mem =False
        else:
            self.mem =True
            
    def on_expanderhd_activate(self, *args):
        if self.hd == True:
            self.hd =False
        else:
            self.hd =True
    def on_info_activate(self, *args):
        if self.info == True:
            self.info =False
        else:
            self.info =True
if __name__ == "__main__":
    a = iclient()
    a.run()
