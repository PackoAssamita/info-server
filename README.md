InfoServer
==========

Graphical information of a remote computer through XML-RPC.

It was just a XML-RPC exercise for learning, don't expect so much.

It hasn't any type of connection security so you must not use it in any 
kind of production environment.

See the screenshot.

The iserver.py needs to be launched by *root*

This software has no configuration files so you must define the variables directly on code, sorry for this.

Authors
-------
Francisco José Moreno Llorca - packo@assamita.net - @kotejante

License
-------

    Copyright (C) 2005-2014 by
      Francisco José Moreno Llorca <packo@assamita.net>

    Infoserver is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    Infoserver is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

